# 자율주행차 위험운전행동 탐지 모듈



## 소개

멀티에이전트 자율주행 시뮬레이터에서 구현된 자율주행차의 운행 데이터를 활용하여 위험운전행동을 탐지할 수 있는 모듈입니다.


## 위험운전행동 탐지 기준

- 자율주행차 운행 데이터 및 도로구간 데이터 수집
- 도로구간별 운행 데이터 Mapping 및 데이터 전처리 수행
- 인공신경망 기반 하이브리드 클러스터링 모델 설계 후 학습 진행
- 각각의 운행패턴 클러스터의 가속도, Yaw Rate 등의 대푯값을 활용하여 위험운전행동 여부 확인
- 기존 연구에서는 상하위 15%에 해당하는 클러스터를 위험운전행동으로 정의 [(Ref. Lee and Jang, 2019)](https://www.sciencedirect.com/science/article/abs/pii/S1369847816306684)


## 데이터셋 상세 구성

- Required: 자율주행차 운행시각, 속도, 가속도, Yaw Rate, 도로구간 ID
- Optional: 브레이크, 차량 조명등, 고도, 센서 및 통신 이상, GPS 등


## 구동 프로그램

- Main: Rstudio (Ver. 2022.07.2+576)
- Sub: R (Ver. 4.2.2)

## 필수 Package

- 모델 설계 및 평가 Package: kohonen, clusterCrit, LICORS, rJAVA
- 수집 및 전처리 Package: rJAVA

cf. [rJAVA 설치 방법](https://r-pyomega.tistory.com/6)


